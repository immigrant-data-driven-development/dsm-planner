import networkx as nx
from matplotlib import pyplot as plt
import pandas as pd
from mdutils.mdutils import MdUtils

class Analytics ():
    def __init__(self, file_path):
        self.file_path = file_path
        self.graph = nx.DiGraph(directed=True)
        self.elements = {}
        self.graph_name = "oi"

        self.mdFile = MdUtils(file_name='./results/{}'.format('READMEDSM'),title = "Analytics")
        self.mdFile.new_header(level=1, title='Relatório Analitico do DSM')
       

        #Reading file
        print ("Reading file")
        self.file = pd.read_excel(self.file_path, sheet_name=None)
        
        self.epic = self.file['EPIC']
        self.elements = {}
        
    def __create_elements(self):
        epics = self.file['EPIC']
        epics = epics.loc[:, ["EPIC", "EPIC-ID"]] 
        
        for element in epics.values.tolist():
            self.elements[element[1]] = element[0]
            


    def __isCycle(self):
        self.ciclos = nx.is_directed_acyclic_graph(self.graph)
        self.is_directed = nx.is_directed(self.graph)

        items = []
        self.ciclos = nx.is_directed_acyclic_graph(self.graph)
        items.append ('É um Grafo Direcionado? &rarr; {}'.format(nx.is_directed(self.graph)))
        items.append ('Há Ciclos no Grafo? &rarr; {}'.format (not self.ciclos))
        self.mdFile.new_list(items)        
        

    def __create_node(self):
        self.epic.drop(['Project','Aspecto','Dominio', 'EPIC', 'Tipo'], axis=1, inplace=True)
        self.epic.replace("x", True,inplace=True)
        self.epic.replace("X", True,inplace=True)
        epic_dict = self.epic.to_dict()
        epic_id_dict = epic_dict['EPIC-ID']
        for key_line, line_value in epic_id_dict.items():
            
            epic_x = epic_dict[line_value]
            for key_column, value_column in epic_x.items():
                if (value_column == True):
                    # criando um edge
                    line = line_value
                    column = epic_id_dict[key_column]
                    self.graph.add_edge(column,line)
                   
    def __draw_graph(self,graph, name):
        plt.figure(4,figsize=(20,20)) 
        pos=nx.spring_layout(graph)
        nx.draw_networkx(graph, pos, node_size=1500, arrows=True)
        plt.savefig('./results/epics/{}.png'.format(name), format="PNG")
        plt.clf()
    
    def __create_node_relationship(self,sorted_topological_graph):

        self.mdFile.new_header(level=2, title='Relação de Dependência e Consequencia entre os elementos')
        
        list_of_strings = ["Elemento", "Qtd Dependencias", "Dependencias","Qtd Habilitados","Habilitados"]
        
        count = 0
        for node in sorted_topological_graph:
            in_edge = self.graph.in_edges(node)
            out_edge = self.graph.out_edges(node)
            list_source = []
            list_target = []
            for edge in in_edge:
                source, target = edge
                list_source.append (source)
            
            for edge in out_edge:
                source, target = edge
                list_target.append (target)

            list_of_strings.extend([node, list_source, str(len(in_edge)), str(len(out_edge)), list_target])

            subgraph = nx.DiGraph(directed=True)
            subgraph.add_edges_from(in_edge)
            subgraph.add_edges_from(out_edge)
            self.__draw_graph(subgraph,"{}".format(node))
            count = count + 1
        
        self.mdFile.new_line()
        self.mdFile.new_table(columns=5, rows=count+1, text=list_of_strings, text_align='center')    

    
    def __print_sorted_node (self, sorted_topological_graph):
        count = 0
        self.mdFile.new_header(level=2, title='Sugestão de Ordem de execução')
        items = []
        for node in sorted_topological_graph:
            if node is not None:
                element = self.elements[node]
                items.append(("{}: {}".format(node, element)))        
                count = count + 1
                del self.elements[node]

        self.mdFile.new_list(items, marked_with='1')

    def __elements_without_graph(self):
        self.mdFile.new_header(level=2, title='Elementos fora do grafo')
        count = 0
        items = []
        if len (self.elements):
            self.mdFile.new_paragraph ("* Nenhum Elemento fora do grafo")
        else:
            for element in self.elements:
                items.append("{}".format(count, element))        
                count = count +1 
        self.mdFile.new_list(items, marked_with='1')

    def __create_cycles(self):
        sub_cycles = sorted(nx.simple_cycles(self.graph))
        count = 0
        
        print ("## Criando os Grafos dos Ciclos")
        
        for cycle in sub_cycles:
            subgraph = nx.DiGraph(directed=True)
            print (subgraph)
            for n in range(len(cycle)-1):
                print ("{}-{}".format(cycle[n], cycle[n+1]))
                subgraph.add_edge(cycle[n], cycle[n+1])   
            
            subgraph.add_edge(cycle[len(cycle)-1], cycle[0])
            count = count + 1
            
            self.__draw_graph(subgraph, 'cycles-'+str(count)) 

    def do(self):
        
        self.__create_elements()
        
        self.__create_node()
        self.__isCycle()
        
        self.mdFile.new_header(level=2, title='Analisando o DSM')
        
        
        if not self.ciclos:
            self.__create_cycles()
        else:
            sorted_topological_graph = list(nx.topological_sort(self.graph))
            self.__create_node_relationship(sorted_topological_graph=sorted_topological_graph)
            self.__print_sorted_node(sorted_topological_graph=sorted_topological_graph)
            self.__elements_without_graph()
            self.__draw_graph(self.graph, "graph-{}".format("project"))
            self.mdFile.new_paragraph("![project](./epics/graph-project.png)")
        self.mdFile.create_md_file()
        