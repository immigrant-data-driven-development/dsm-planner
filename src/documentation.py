import pandas as pd

class Documentation():

    def __init__(self, us, task) -> None:
        self.us = us
        self.task = task

    def do (self):
        self.__create_epic_md()
        self.__create_us_md()
        
    def __create_file(self, file_name, value):
        text_file = open(file_name, "w")
        text_file.write(value)
        text_file.close()
    
    def __create_epic_md(self):
        epics = self.us.loc[:, ["EPIC", "Tipo","EPIC-ID"]] 
        epics = epics.drop_duplicates() 
        epics['Link'] = epics.apply(lambda data: "[link](./epics/{}.md)".format(data["EPIC-ID"]),axis=1)
         
        markdown = "# Visão Geral do Projeto \n\n"
        markdown = markdown + '## Planejamento \n\n' 
        markdown = markdown + "Neste [documento](READMEDSM.md) há sugestão de execução do projeto. \n\n"
        markdown = markdown + "## Epics do Projeto \n\n" 
        markdown = markdown + epics.to_markdown(index=False)
        self.__create_file("./results/README.md",markdown)

    def __create_task_section_us_file(self, us_id):
        df_result = self.task.loc[self.task["US-ID"]==us_id]
        task = df_result.loc[:, ["TASK","TASK-ID"]]
        return task.to_markdown(index=False)

    def __create_us_file(self, epic, epic_id):
        
        file_name = "./results/epics/{}.md".format(epic_id)
        df_result = self.us.loc[self.us["EPIC-ID"]==epic_id]
        us = df_result.loc[:, ["US","US-ID"]]

        markdown = "# [{}] - {} \n".format(epic_id, epic)
        markdown = markdown + us.to_markdown(index=True)
        markdown = markdown + "\n\n"
        markdown = markdown + "## Relação entre as histórias de usuário \n\n"
        markdown = markdown + "![graph]({}.png) \n\n".format(epic_id.upper())

        markdown = markdown + "\n\n"

        us_list = us.values.tolist()
        
        for element in us_list:
            markdown = markdown + "## [{}] - {} \n\n".format(element[1],element[0]) + "\n"
            markdown = markdown + self.__create_task_section_us_file(element[1]) + "\n\n"
        
        self.__create_file(file_name,markdown)

    def __create_us_md(self):
        epic = self.us.loc[:, ["EPIC", "EPIC-ID"]]
        epic = epic.drop_duplicates() 
        epic.apply(lambda data: self.__create_us_file(data["EPIC"], data["EPIC-ID"]), axis=1)
        
        
    