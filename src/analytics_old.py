import networkx as nx
from matplotlib import pyplot as plt
from mdutils.mdutils import MdUtils
from openpyxl import load_workbook

class Analytics ():

    def __init__(self,file_path):
        self.file_path = file_path
        self.wb = load_workbook(filename=file_path)
        self.graph = nx.DiGraph(directed=True)
        self.elements = {}

        #Pegando o Sheet do EPIC
        self.sheet = self.wb.sheet_by_index(0)
        self.mdFile = None
        self.graph_name = None
        

    def __create_elements(self):
        print ("Reading File")
        for linha in range(self.sheet.nrows):
            index = self.sheet.cell_value(linha, 5)
            value = self.sheet.cell_value(linha, 3)            
            self.elements [index] = value
    
    def __create_graph(self):
        print ("Criando o Grafo")
        for i in range(1,self.sheet.nrows):
            for j in range(6,self.sheet.ncols):
                value = self.sheet.cell_value(i, j) 
                if value is not None:
                    value = value.lower()
                    if value == 'x': 
                        linha = (self.sheet.cell_value(i, 5))
                        coluna = (self.sheet.cell_value(0, j))
                        self.graph.add_edge(linha, coluna)

    def __draw_graph(self,graph, name):
        plt.figure(4,figsize=(20,20)) 
        pos=nx.spring_layout(graph)
        nx.draw_networkx(graph, pos, node_size=1500, arrows=True)
        plt.savefig('./results/epics/{}.png'.format(name), format="PNG")
        plt.clf()

    def __create_cycles(self):
        sub_cycles = sorted(nx.simple_cycles(self.graph))
        count = 0
        
        print ("## Criando os Grafos dos Ciclos")
        
        for cycle in sub_cycles:
            subgraph = nx.DiGraph(directed=True)
            
            for n in range(len(cycle)-1):
                subgraph.add_edge(cycle[n], cycle[n+1])   
            
            subgraph.add_edge(cycle[len(cycle)-1], cycle[0])
            count = count + 1
            
            self.__draw_graph(subgraph, 'subgraph-'+str(count)) 
    
    def __create_node_relationship(self,sorted_topological_graph):

        self.mdFile.new_header(level=2, title='Relação de Dependência e Consequencia entre os elementos')
        
        list_of_strings = ["Elemento", "Qtd Dependencias", "Dependencias","Qtd Habilitados","Habilitados"]
        count = 0
        for node in sorted_topological_graph:
            in_edge = self.graph.in_edges(node)
            out_edge = self.graph.out_edges(node)
            list_source = []
            list_target = []
            for edge in in_edge:
                source, target = edge
                list_source.append (source)
            
            for edge in out_edge:
                source, target = edge
                list_target.append (target)
            list_of_strings.extend([node, list_source, str(len(in_edge)), str(len(out_edge)), list_target])

            subgraph = nx.DiGraph(directed=True)
            subgraph.add_edges_from(in_edge)
            subgraph.add_edges_from(out_edge)
            self.__draw_graph(subgraph,"{}-subgraph-{}".format(self.graph_name,node))
            count = count + 1

        self.mdFile.new_line()
        self.mdFile.new_table(columns=5, rows=count+1, text=list_of_strings, text_align='center')    

        
    def __print_sorted_node (self, sorted_topological_graph):
        count = 0
        self.mdFile.new_header(level=2, title='Sugestão de Ordem de execução')
        items = []
        for node in sorted_topological_graph:
            if node is not None:
                element = self.elements[node]
                items.append(("{}: {}".format(node, element)))        
                count = count + 1
                del self.elements[node]

        self.mdFile.new_list(items, marked_with='1')

    def __elements_without_graph(self):
        self.mdFile.new_header(level=2, title='Elementos fora do grafo')
        count = 0
        items = []
        if len (self.elements):
            self.mdFile.new_paragraph ("* Nenhum Elemento fora do grafo")
        else:
            for element in self.elements:
                items.append("{}".format(count, element))        
                count = count +1 
        self.mdFile.new_list(items, marked_with='1')
    
    def report (self):            
        self.__create_elements()
        self.__create_graph()
        graph_name = self.sheet.name.lower().replace(" ","_")
        self.graph_name = graph_name
        self.mdFile = MdUtils(file_name='./results/{}'.format('dsm'),title = self.sheet.name)
        self.mdFile.new_header(level=1, title='Relatório Analitico do DSM: {}'.format(self.sheet.name))
        self.mdFile.new_header(level=2, title='Analisando o DSM')
        
        items = []
        ciclos = nx.is_directed_acyclic_graph(self.graph)
        items.append ('É um Grafo Direcionado? &rarr; {}'.format(nx.is_directed(self.graph)))
        items.append ('Há Ciclos no Grafo? &rarr; {}'.format (not ciclos))
        self.mdFile.new_list(items)        
        
        if not ciclos:
            self.__create_cycles()
        else:
            sorted_topological_graph = list(nx.topological_sort(self.graph))
            self.__create_node_relationship(sorted_topological_graph=sorted_topological_graph)
            self.__print_sorted_node(sorted_topological_graph=sorted_topological_graph)
            self.__elements_without_graph()
            self.__draw_graph(self.graph, "graph-{}".format(graph_name))
        
        self.mdFile.create_md_file()
        
