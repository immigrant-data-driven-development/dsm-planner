import pandas as pd
from openpyxl import load_workbook


class Planning ():

    def __init__(self, file_path):
        
        self.file_path = file_path
        
        #Reading file
        print ("Reading file: {}".format(self.file_path))
        self.file = pd.read_excel(self.file_path, sheet_name=None)
        #Reading sheets
        self.epic = self.file['EPIC']
        self.meta_us = self.file['Meta US']
        self.meta_task = self.file['Meta Task']
        
        
    def __create_task_by_us(self, us,us_id, meta_id):
        
        df_tipo = self.meta_task.loc[self.meta_task["META-US-ID"]==meta_id]
        
        df_result = pd.DataFrame()
        df_result["TASK"] = df_tipo.loc[:,["Task"]].apply(lambda data: data,axis=1)
        df_result['US'] =  us
        df_result['META-US-ID'] = meta_id
        df_result['US-ID'] =  us_id
        df_result["TASK-ID"] = us_id
        df_result["ACTIVITY"] = df_tipo['Activity']
        df_result["TAG"] = df_tipo['Aspect']
        
        
        return df_result.values
        
    def __create_task(self):
        print ("Creating TASKS")
        tasks = self.us.loc[:, ["US","US-ID", "META-US-ID"]]
        df_result = tasks.apply(lambda us: self.__create_task_by_us(us['US'], us['US-ID'], us['META-US-ID']), axis=1)
        
        
        data = self.__list_2_list(df_result,4)

        self.task = pd.DataFrame(data, columns=['TASK', 'US','Tipo', 'US-ID', 'TASK-ID','ACTIVITY', 'TAG'])
        self.task = self.task [['US','US-ID','Tipo','TASK', 'TASK-ID','ACTIVITY', 'TAG']]
            
    def __create_user_story_by_epic(self, epic, epic_id, tipo):
       
        # Select Meta US com o tipo
        df_tipo = self.meta_us.loc[self.meta_us["Tipo"]==tipo]
        
        df_result = pd.DataFrame()
        df_result['US'] = df_tipo.loc[:,["Keyword", "US"]].apply(lambda data: epic.replace(data['Keyword'], data['US']),axis=1)
        df_result['US-ID'] =  epic_id
        df_result['EPIC'] =  epic
        df_result['EPIC-ID'] =  epic_id
        df_result['Tipo'] =  tipo
        df_result['META-US-ID'] = df_tipo['META-US-ID']
            
        return df_result.values

    def __create_user_story(self):
        #Create User Story combining EPIC and Meta US
        epics = self.epic.loc[:, ["EPIC", "Tipo","EPIC-ID"]] 
        
        # Creating the collumns
        print ("Creating US ...")
        df_result = epics.apply(lambda data: self.__create_user_story_by_epic(data["EPIC"], data["EPIC-ID"], data["Tipo"]), axis=1)
        
        #Convertendo a lista de lista em uma lista
        data = self.__list_2_list(df_result,1)
     
        self.us = pd.DataFrame(data, columns=['US', 'US-ID','EPIC','EPIC-ID','Tipo','META-US-ID'])
        self.us = self.us [['EPIC','Tipo','EPIC-ID','US','US-ID','META-US-ID']]
    
    def __list_2_list(self, df_result, index ):
        
        data = []
        for line in df_result:
            count = 0
            for element in line:
                element [index] = element [index] + "-{}".format(count)
                data.append (element)
                count = count + 1
                
        return data 

    def update_excel(self):
        print ("Updating Excel File")
        book = load_workbook(self.file_path)
        writer = pd.ExcelWriter(self.file_path, engine='openpyxl')
        writer.book = book
        
        self.us.drop(['META-US-ID'], axis=1)
        
        self.us.to_excel(writer,sheet_name="US",index=False)
        self.task.to_excel(writer,sheet_name="TASK",index=False)
        writer.close()

    def do (self):
        self.__create_user_story()
        self.__create_task()
        self.update_excel()
        return self.us, self.task