
#import xlrd
from analytics import Analytics
from documentation import Documentation
from planning import Planning
from ms_devops import MSDevOPS

file_path = './dsm_file/dsm-immigrant-mdd-daniel.xlsx'

#criando o planejamento
planning = Planning(file_path=file_path)
us, task = planning.do()

#analisando o planejamento
analytics = Analytics(file_path=file_path)
analytics.do()

# Criando a documentação 
documentation = Documentation(us, task)
documentation.do()

#Criando o arquivo ou enviando parao devops
msdevops = MSDevOPS(us, task)
msdevops.do()