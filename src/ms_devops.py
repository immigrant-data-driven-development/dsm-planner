import csv
class MSDevOPS():

    def __init__(self, us = None, tasks=None):
        self.header = ['Work Item Type','State','Title 1','Title 2','Title 3','Tags', 'Activity']
        self.us = us
        self.tasks = tasks
    
    def __create_task(self, name, task_id, activity, tag ):
        
        task_csv = {
                'Work Item Type': 'Task',
                'State':'To Do',
                'Title 3':'[{}]-{}'.format(task_id,name),
                'Tags':'{}'.format(tag),
                'Activity': activity
        }

        self.writer.writerow(task_csv)
    
    def __create_us(self, us, us_id,tipo):
        
        us_csv = {
                'Work Item Type': 'Product Backlog Item',
                'State':'New',
                'Title 2':'[{}] - {}'.format(us_id,us),
                'Tags':'{}'.format(tipo)
            }
        self.writer.writerow(us_csv)
        tasks = self.tasks.loc[:, ["TASK", "US-ID","TASK-ID", "ACTIVITY", "TAG"]]
        df_result = tasks.loc[tasks["US-ID"]==us_id]
        df_result.apply(lambda data: self.__create_task(data["TASK"], data["TASK-ID"], data["ACTIVITY"], data["TAG"]),axis=1) 
        

    def __create_epic(self, epic, tipo, epic_id):
        
        epic = {
            'Work Item Type': 'Epic',
            'State':'New',
            'Title 1':'[{}] - {}'.format(epic_id,epic.strip()),
            'Tags':'{}'.format(tipo)
        }
        self.writer.writerow(epic)  
        #remover essa linha e colocar mais acima
        us = self.us.loc[:, ["US", "US-ID","EPIC-ID"]]
        df_result = us.loc[us["EPIC-ID"]==epic_id]
        df_result.apply(lambda data: self.__create_us(data["US"], data["US-ID"],tipo),axis=1) 
        


    def __create_content(self):
        epics = self.us.loc[:, ["EPIC", "Tipo","EPIC-ID"]] 
        epics = epics.drop_duplicates() 
        
        epics.apply(lambda data: self.__create_epic(data["EPIC"], data["Tipo"], data["EPIC-ID"]),axis=1)
          
           

    def do(self):
        with open('./results/application_management.csv', 'w', encoding='UTF8', newline='') as f:
            self.writer = csv.DictWriter(f, fieldnames=self.header)
            
            self.writer.writeheader()
        
            self.__create_content()
            
        f.close()
